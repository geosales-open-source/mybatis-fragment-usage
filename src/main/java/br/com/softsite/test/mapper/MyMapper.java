package br.com.softsite.test.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;

public interface MyMapper {

	@Insert("INSERT INTO A (B) VALUES (#{param})")
	void inserirA(String p);

	void inserirB(@Param("paramB") String paramB, @Param("paramC") String paramC);
	void inserirC(@Param("paramB") String paramB, @Param("paramC") String paramC);
	void inserirD(@Param("marmA") String marmA, @Param("marmB") String marmB);
	void inserirSemBinding(@Param("marmA") String marmA, @Param("marmB") String marmB);
}
