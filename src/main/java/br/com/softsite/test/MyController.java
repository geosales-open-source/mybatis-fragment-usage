package br.com.softsite.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zaxxer.hikari.HikariDataSource;

import br.com.softsite.test.mapper.MyMapper;

@RestController
public class MyController {

	@Autowired HikariDataSource dataSource;
	@Autowired MyMapper mapper;

	@PostMapping(path = "test/{param}", produces = "text/plain")
	public String insere(@PathVariable("param") String param) {
		System.out.println("param " + param);
		mapper.inserirA(param);

		return param;
	}

	@PostMapping(path = "testB/{paramB}/{paramC}", produces = "text/plain")
	public String insereB(@PathVariable("paramB") String paramB, @PathVariable("paramC") String paramC) {
		System.out.println(String.format("paramA %s paramB %s", paramB, paramC));
		mapper.inserirC(paramB, paramC);

		return paramB + "/" + paramC;
	}

	@PostMapping(path = "testC/{paramB}/{paramC}", produces = "text/plain")
	public String insereC(@PathVariable("paramB") String paramB, @PathVariable("paramC") String paramC) {
		System.out.println(String.format("paramA %s paramB %s", paramB, paramC));
		mapper.inserirC(paramB, paramC);

		return paramB + "/" + paramC;
	}

	@PostMapping(path = "testD/{marmA}/{marmB}", produces = "text/plain")
	public String insereD(@PathVariable("marmA") String marmA, @PathVariable("marmB") String marmB) {
		System.out.println(String.format("paramA %s marmA %s", marmA, marmB));
		mapper.inserirD(marmA, marmB);

		return marmA + "/" + marmB;
	}

	@PostMapping(path = "testFalha/{marmA}/{marmB}", produces = "text/plain")
	public String insereFalha(@PathVariable("marmA") String marmA, @PathVariable("marmB") String marmB) {
		System.out.println(String.format("paramA %s marmA %s", marmA, marmB));
		mapper.inserirSemBinding(marmA, marmB);

		return marmA + "/" + marmB;
	}

	@PostConstruct
	public void base() {
		try {
			try (Connection c = dataSource.getConnection();
					Statement stmt = c.createStatement()) {
				stmt.executeUpdate("CREATE TABLE IF NOT EXISTS A (B, C)");
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}
